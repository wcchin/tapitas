
"""
modified (extended) from code snippets:
http://code.activestate.com/recipes/325823-draw-svg-images-in-python/

SVG.py - Construct/display SVG scenes.

The following code is a lightweight wrapper around SVG files. The metaphor
is to construct a scene, add objects to it, and then write it to a file
to display it.

This program uses ImageMagick to display the SVG files. ImageMagick also
does a remarkable job of converting SVG files into other formats.
"""

import os
import numpy as np
display_prog = "display" # Command to execute to display images.

class Layer:
    def __init__(self):
        self.items = []

    def addtoLayer(self, item):
        self.items.append(item)

class Scene:
    def __init__(self,name="svg",title="", height=400,width=400,
        bbox=[np.nan, np.nan, np.nan, np.nan], color=(255,255,255),
        opacity=0.8, strokecolor=(0,0,0), strokewidth=0.2,
        backgroundcolor="#b3c0ef", prettyprint=True):
        self.name = name
        self.title = title
        self.items = []
        self.styleitems = []
        self.layers={}
        self.height = height
        self.width = width
        self.bbox = bbox
        self.color = colorstr(color)
        self.opacity = opacity
        self.strokecolor = colorstr(strokecolor)
        self.strokewidth = strokewidth
        self.backgroundcolor = colorstr(backgroundcolor)
        self.prettyprint = prettyprint
        self.init_style()
        return

    def updatebbox(self, newbbox):
        if np.isnan(self.bbox[0]):
            self.bbox = newbbox
        else:
            xmin,ymin,xmax,ymax  = self.bbox
            xmin2,ymin2,xmax2,ymax2  = newbbox
            if xmin2<xmin:
                xmin=xmin2
            if ymin2<ymin:
                ymin=ymin2
            if xmax2>xmax:
                xmax=xmax2
            if ymax2>ymax:
                ymax=ymax
            self.bbox = [xmin,ymin,xmax,ymax]

    def addLayer(self, layertuple):
        key, layer = layertuple
        if key in self.layers:
            raise ValueError('sequence must be unique')
        else:
            self.layers.update({key: layer})

    def listinglayers(self):
        self.items = []
        seq_list = []
        for layer_seq in self.layers:
            seq_list.append(layer_seq)
        seq_list.sort()
        for key in seq_list:
            self.items.extend(self.layers[key].items)

    # artifact
    def add(self,item): self.items.append(item)

    def init_style(self):
        self.styleitems.append('<style type="text/css" media="screen">\n')
        self.styleitems.append("svg {background: %s;}\n"%(self.backgroundcolor))
        self.styleitems.append('<![CDATA[\n')
        #self.styleitems.append("polyline:hover{opacity: 0.8; stroke-width:200;}\n")
        #self.styleitems.append("polygon:hover{opacity: 0.8; stroke-width:200;}\n")
        self.styleitems.append("]]>\n</style>\n")
        """
        styles = "<style>\n"
        styles = styles + "svg {background: %s;}\n"%(self.backgroundcolor)
        styles = styles + "polyline:hover{opacity: 0.8; stroke-width:200;}\n"
        styles = styles + "polygon:hover{opacity: 0.8; stroke-width:200;}\n"
        styles = styles + "</style>\n"
        #return styles
        """

    def updatestyle(self, classkey="", featuretype="",hovercolor=None,hoveropacity=None,hoverstroke=None,hoverswidth=None):

        self.styleitems.insert(len(self.styleitems)-1, '.%s :hover {\n'%(classkey))
        if hovercolor is not None:
            self.styleitems.insert(len(self.styleitems)-1, 'fill: %s;\n'%(colorstr(hovercolor)))
        if hoveropacity is not None:
            self.styleitems.insert(len(self.styleitems)-1, 'opacity: %s;\n'%(hoveropacity))
        if hoverstroke is not None:
            #print hoverstroke
            self.styleitems.insert(len(self.styleitems)-1, 'stroke:%s;\n'%(colorstr(hoverstroke)))
        if hoverswidth is not None:
            self.styleitems.insert(len(self.styleitems)-1, 'stroke-width:%s;'%(hoverswidth))
        self.styleitems.insert(len(self.styleitems)-1, '}\n')

    def get_styles(self):
        styles = ""
        for item in self.styleitems: styles += item
        return styles

    def map_elements(self,xmin,ymin,boxheight,boxwidth):
        strs = ["<g id='header'>\n", ]
        title = Text(anchor_position=["95%","8%"],text=self.title,size=56,anchor_point="end")
        strs.extend(title.strarray())
        strs.append("</g>\n")
        me = " ".join(strs)
        return me

    def strarray(self):
        #print self.bbox
        self.listinglayers()
        xmin,ymin,xmax,ymax  = self.bbox
        boxwidth = xmax-xmin
        boxheight = ymax-ymin
        styles = self.get_styles()
        map_elements = self.map_elements(xmin,ymin,boxheight,boxwidth)
        var = ["<?xml version=\"1.0\"?>\n",
               "<svg version=\"1.2\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink= \"http://www.w3.org/1999/xlink\" xmlns:ev=\"http://www.w3.org/2001/xml-events\"  baseProfile=\"tiny\" \n",
               "width=\"%s\" height=\"%s\">\n" % (self.width,self.height),
               map_elements,
               "<svg width=\"%s\" height=\"%s\" viewBox=\"%r %r %r %r\" preserveAspectRatio=\"xMidYMid meet\" viewport-fill=\"%s\">\n" % ("100%","100%", xmin, ymin, boxwidth, boxheight, self.backgroundcolor),
               styles,
               "<script xlink:href=\"SVGPan.js\" />\n<g id=\"viewport\">\n",
               "<g transform=\"translate(%r %r) scale(1,-1)\">\n" %(0, 2*ymin+boxheight),#%(-xmin, ymin+boxheight),
               "<g style=\"fill: %s; fill-opacity:%s; " %(self.color, self.opacity),
               "stroke:%s; stroke-width:%s;\">\n"%(self.strokecolor, self.strokewidth)]
        for item in self.items: var += item.strarray()
        var += ["</g>\n</g>\n</g>\n</svg>\n</svg>\n"]
        return var

    def write_svg(self,filename=None):
        if filename is None:
            filename="map_01.svg"
        file = open(filename,'w')
        file.writelines(self.getsvg())
        file.close()
        return

    def display(self,prog=display_prog):
        os.system("%s %s" % (prog,self.svgname))
        return

    def getsvg(self):
        strings="".join(self.strarray())
        if self.prettyprint:
            return strings
        else:
            return strings.replace("  "," ").replace("\n", " ")

class initLayer:
    def __init__(self, key='layername', title='Layer label', dcolor=None,
        dopacity=None, dscolor=None, dswidth=None, dsarray=None):
        self.key = key
        self.dcolor = dcolor
        self.dopacity = dopacity
        self.dscolor = dscolor
        self.dswidth = dswidth
        self.dsarray = dsarray
        self.title = title
        return

    def strarray(self):
        string = '<g class="%s" style=\"' % (self.key)
        if self.dcolor != None:
            string = string + 'fill: %s; ' % (colorstr(self.dcolor))
        if self.dopacity != None:
            string = string + 'fill-opacity: %s; ' % (self.dopacity)
        if self.dscolor != None:
            string = string + 'stroke: %s; ' % (colorstr(self.dscolor))
        if self.dswidth != None:
            string = string + 'stroke-width: %s; ' % (self.dswidth)
        if self.dsarray != None:
            string = string + 'stroke-dasharray: %s; ' % (self.dsarray)
        string = string + '\">\n'
        string = string + '<title> %s </title>\n' % (self.title)
        return [string]

class closeLayer:
    def __init__(self):
        #pass
        return

    def strarray(self):
        string = "</g>\n"
        return [string]

class Line:
    def __init__(self,start,end,color=None,strokewidth=None):
        self.start = start #xy tuple
        self.end = end     #xy tuple
        self.color = color
        self.strokewidth = strokewidth
        return

    def strarray(self):
        strs = ["  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" " %\
                (self.start[0],self.start[1],self.end[0],self.end[1])]
        if not (self.strokewidth is None):
            self.strokewidth = '2'
        if self.color is None:
            self.color = (255,0,0)
        rr,gg,bb = self.color
        strs.append('style="stroke:rgb(%s,%s,%s);stroke-width:%s"/>\n' % (str(rr),str(gg),str(bb),str(self.strokewidth)))
        return strs

class simplePath:
    def __init__(self,start,end, path_id='path-id', stroke_width=2, stroke_color='#d3d3d3', stroke_opacity=0.8, rx=120, ry=20, xrotate=0, large_arc=0, sweep_flag=1):
        self.start = start #xy tuple
        self.end = end     #xy tuple
        self.rx = rx
        self.ry = ry
        self.xrotate = xrotate
        self.large_arc = large_arc # 1 large, 0 small
        self.sweep_flag = sweep_flag # 1 time, 0 anti-time
        self.sc = colorstr(stroke_color)
        self.sw = stroke_width
        self.so = stroke_opacity
        self.path_id = path_id
        return

    def strarray(self):
         return ["<path d=\"M %d,%d A%d,%d %d %d,%d %d %d \" stroke=\"%s\" stroke-width=\"%s\" stroke-opacity=\"%s\" fill=\"none\" id=\"%s\" />" %\
          (self.start[0], self.start[1], self.rx, self.ry, self.xrotate, self.large_arc, self.sweep_flag, self.end[0], self.end[1], self.sc, self.sw, self.so, self.path_id)]

        #return ["  <line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" />\n" % (self.start[0],self.start[1],self.end[0],self.end[1])]

class linePath:
    def __init__(self, shpstr, path_id='path-id', stroke_width=2, stroke_color='#d3d3d3', stroke_opacity=0.8, fill_color='FFF'):
        self.shpstr = shpstr[:-1] #result from get_pathstring
        self.sc = colorstr(stroke_color)
        self.sw = stroke_width
        self.so = stroke_opacity
        self.fill = colorstr(fill_color)
        self.path_id = path_id
        return

    def strarray(self):
         return ['<path d="%s" stroke="%s" stroke-width="%s" stroke-opacity="%s" fill="%s" id="%s" /> \n' %\
          (self.shpstr, self.sc, self.sw, self.so, self.fill, self.path_id)]


class MultiPolygons:
    def __init__(self, key=0, label='label', multiPolygons=None, color=None, opacity=None):
        self.key = key
        if label != 'label':
            self.label = str(label)
        else:
            self.label = str(key)
        self.multiPolygons = multiPolygons # list of xy tuple
        if color!=None:
            self.color = colorstr(color)
        else:
            self.color = None
        self.opacity = opacity
        return

    def strarray(self):
        string = '<g fill-rule="evenodd" id="%s" style=" ' %(self.label)
        if self.color != None:
            string = string + 'fill: %s; ' % (self.color)
        if self.opacity != None:
            string = string + 'fill-opacity: %s; ' % (self.opacity)
        string = string + '">'+'<title>'+self.label+'</title>'
        """
        for polygon in self.multiPolygons:
            string=string+'<polygon points="'
            for point in polygon:
                string = string + str(point[0])+ ","+str(point[1]) + "  "
            string = string+'" />'
        string = string+'</g>\n'
        """
        string=string+'<path d="'
        for polygon in self.multiPolygons:
            string=string+'M '
            for point in polygon:
                string = string + str(point[0])+ ","+str(point[1]) + " L "
            string = string[:-2]+'Z '
        string = string+'" />'
        string = string+'</g>\n'
        #print string
        return [string]

class MultiPolylines:
    def __init__(self, key=0, label='label', multiPolylines=None, strokecolor=None, strokewidth=None):
        self.key = key
        if label != 'label':
            self.label = str(label)
        else:
            self.label = str(key)
        self.multiPolylines = multiPolylines # list of xy tuple
        if strokecolor != None:
            self.strokecolor = colorstr(strokecolor)
        else:
            self.strokecolor = None
        self.strokewidth = strokewidth
        return

    def strarray(self):
        string = '<g id="%s" style="' %(self.key)
        if self.strokecolor != None:
            string = string + 'stroke: %s; ' % (self.strokecolor)
        if self.strokewidth != None:
            string = string + 'stroke-width: %s; ' % (self.strokewidth)
        string = string + '">'+'<title>'+self.label+'</title>'
        if string[-3:-1] == '\"\"':
            string = string[0:-10]+">"
        for polyline in self.multiPolylines:
            string=string+'<polyline points="'
            for point in polyline:
                string = string + str(point[0])+ ","+str(point[1]) + "  "
            string = string[:-2]+'" />'
        string = string+'</g>\n'
        #print string
        return [string]

class Circle:
    def __init__(self,center,radius,color, idd=""):
        self.center = center #xy tuple
        self.radius = radius #xy tuple
        self.color = color   #rgb tuple in range(0,256)
        self.id = idd
        return

    def strarray(self):
        return ["  <circle cx=\"%d\" cy=\"%d\" r=\"%d\"" %\
                (self.center[0],self.center[1],self.radius),
                "  fill=\"%s\" id=\"%s\" />\n" % (colorstr(self.color), self.id)]

class Rectangle:
    def __init__(self,origin,width,height,color,opacity):
        self.origin = origin
        self.height = height
        self.width = width
        self.color = color
        self.opacity = opacity
        return

    def strarray(self):
        return ["  <rect x=\"%s\" y=\"%s\" height=\"%s\"" %\
                (self.origin[0],self.origin[1],self.height),
                " width=\"%s\" style=\"fill:%s; fill-opacity:%s\" />\n" %\
                (self.width,colorstr(self.color),self.opacity)]

class Text:
    def __init__(self,anchor_position=[0,0],text="Text",size=24,anchor_point="start"):
        self.origin = anchor_position
        self.text = text
        self.size = size
        self.anchor_point = anchor_point
        return

    def strarray(self):
        return ["  <text x=\"%s\" y=\"%s\" font-size=\"%d\" text-anchor=\"%s\">\n" %\
                (self.origin[0],self.origin[1],self.size, self.anchor_point),
                "   %s\n" % self.text,
                "  </text>\n"]


class Appear_Circle():
    def __init__(self,center,radius,color, idd="", t0=1, t1=2, t2=3):
        self.dur1 = str(t1-t0+1)+'s'
        self.dur2 = str(t2-t1)+'s'
        self.begin1 = str(t0-1)+'s'
        self.begin2 = str(t1)+'s'
        self.center = center #xy tuple
        self.radius = radius #xy tuple
        self.color = color   #rgb tuple in range(0,256)
        self.id = idd
        return

    def strarray(self):
        strs = ['<circle cx="%d" cy="%d" r="%d"' %\
                (self.center[0],self.center[1],self.radius),
                "fill=\"%s\" opacity=\"0\" id=\"%s\" >\n" % (colorstr(self.color), self.id),
                '<animate id="in_%s" attributeType="CSS" attributeName="opacity" from="0" to="1" begin="%s" dur="%s" restart="never" repeatCount="1" /> \n <animate id="out_%s" attributeType="CSS" attributeName="opacity" from="1" to="0" begin="%s" dur="%s" restart="never" repeatCount="1" /> \n' % (self.id, self.begin1, self.dur1, self.id, self.begin2, self.dur2),
                '</circle>\n'
                ]
        return strs


class Morphing_Shapes():
    def __init__(self, idd='', init_str='', begin=5, total_time=10, fading_duration=30, color=(200,200,200)):
        self.total_time = str(total_time)+'s'
        self.fadingdur = str(fading_duration)+'s'
        self.begin = str(begin)+'s'
        self.begin0 = str(begin-1)+'s'
        self.last = str(begin+total_time)+'s'
        self.fillcolor = colorstr(color)
        self.idd = idd
        self.strs = []
        self.keyTimes = []
        self.keySplines = []
        self.vertexes = []
        """
        x0, y0 = init_v[0]
        temp = 'M '+"%.6f" % x0+','+"%.6f" % y0+' '
        for xx,yy in init_v[1:]:
            ss = 'L '+"%.6f" % xx+','+"%.6f" % yy+' '
            temp = temp + ss
        temp = temp + 'Z'
        """
        self.initshp = init_str

    def add_shape(self, vertexstr='', tt=0.25):
        tx = "%.3f" % tt
        self.keyTimes.append(str(tx))
        self.keySplines.append('.5,0,.5,1')
        """
        x0, y0 = vertexlist[0]
        temp_strs = 'M '+"%.6f" % x0+','+"%.6f" % y0+' '
        for xx,yy in vertexlist[1:]:
            ss = 'L '+"%.6f" % xx+','+"%.6f" % yy+' '
            temp_strs = temp_strs + ss
        temp_strs = temp_strs+'Z ; \n'
        """
        self.vertexes.append(vertexstr)

    def strarray(self):
        vstrs = ' '.join(self.vertexes)[:-2]
        strs = ['<path d="%s" style="fill:%s; stroke:black; stroke-width:0; fill-opacity:0;">\n' % (self.initshp, self.fillcolor),
        '<animate id="in_%s" attributeType="CSS" attributeName="fill-opacity" from="0" to="1" begin="%s" dur="%s" repeatCount="1" fill="freeze" /> \n' % (self.idd, self.begin0, '1s'),
        '<animate id="morph_%s" dur="%s"  repeatCount="1" begin="in_%s.end"' % (self.idd, self.total_time, self.idd),
        'keyTimes="%s" calcMode="spline"' % ';'.join(self.keyTimes),
        'keySplines="%s"' % ';'.join(self.keySplines[:-1]),
        'attributeName="d" \n values=\n"%s"' % vstrs,'\n fill="freeze" /> \n',
        '<animate id="out_%s" attributeType="CSS" attributeName="fill-opacity" from="1" to="0" begin="morph_%s.end" dur="%s" repeatCount="1" fill="freeze" /> \n ' % (self.idd, self.idd, self.fadingdur),
        '</path>\n']
        return strs

def get_pathstring(vertexlist=[]):
    x0, y0 = vertexlist[0]
    temp_strs = 'M '+"%.6f" % x0+','+"%.6f" % y0+' '
    for xx,yy in vertexlist[1:]:
        ss = 'L '+"%.6f" % xx+','+"%.6f" % yy+' '
        temp_strs = temp_strs + ss
    temp_strs = temp_strs+'Z ;'
    return temp_strs

class animated_line():
    def __init__(self, idd='', start=(), end=(), t0=1, t1=5):
        self.start = start #xy tuple
        self.end = end     #xy tuple
        x0,y0 = start
        x1,y1 = end
        dur = t1-t0+1
        begin = t0-1
        self.strs = [' <line x1="%d" y1="%d" x2="%d" y2="%d"  style="stroke:rgb(180,0,0);stroke-width:20">\n' %                 (x0,y0,x0,y0)]
        dx = (x1-x0)/float(dur)
        dy = (y1-y0)/float(dur)
        beg = 0
        for i in range(int(np.ceil(dur-1))):
            j=i+1
            xnew = x0 + j*dx
            ynew = y0 + j*dy
            beg = j+begin
            self.strs.append('<set attributeName="x2" to="%d" begin="%s" /> ' % (xnew, str(beg)+'s'))
            self.strs.append('<set attributeName="y2" to="%d" begin="%s" /> \n' % (ynew, str(beg)+'s'))
        self.strs.extend(['<animate id="fadeout_%s" attributeType="CSS" attributeName="opacity" from="1" to="0" begin="%s" dur="%s" restart="never" fill="freeze"/> \n' % (idd, beg, 1),
        '</line>\n'])

    def strarray(self):
        return self.strs

def colorstr(rgb):
    if rgb[0]!="#":
        return "#%x%x%x" % (rgb[0]/16,rgb[1]/16,rgb[2]/16)
    else:
        return rgb

def test():
    scene = Scene('test')
    scene.add(Rectangle((100,100),200,200,(0,255,255),0.8))
    scene.add(Line((200,200),(200,300)))
    scene.add(Line((200,200),(300,200)))
    scene.add(Line((200,200),(100,200)))
    scene.add(Line((200,200),(200,100)))
    scene.add(Circle((200,200),30,(0,0,255)))
    scene.add(Circle((200,300),30,(0,255,0)))
    scene.add(Circle((300,200),30,(255,0,0)))
    scene.add(Circle((100,200),30,(255,255,0)))
    scene.add(Circle((200,100),30,(255,0,255)))
    scene.add(MultiPolygons(multiPolygons=[[[50,50], [75,50],[50,75]]], color=(0,0,255)))
    scene.add(initLayer(key='layername', dopacity=0.8, dscolor=(255,0,0), dswidth=2.0))
    scene.add(MultiPolylines(multiPolylines=[[[50,50], [135,155], [175,50],[150,75]]]))
    scene.add(closeLayer())
    scene.add(Text((50,50),"Testing SVG"))
    #scene.write_svg()
    #scene.display()
    return " ".join(scene.strarray())

if __name__ == '__main__':
    print test()
