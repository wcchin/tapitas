# -*- coding: utf-8 -*-
import os
import jinja2
import svg
import pandas as pd
import numpy as np
from datetime import datetime
import time

def render_svg(param_dict={}, drawing=None, CDATA=None, textbox=None, footer=None):
    default_param_dict = dict(title="Test example", description="simple description", title_in_page="Hello World", canvas_width="100%", canvas_height="100%", xmin=146693.95, ymin=2422327.01, boxwidth=204124.36, boxheight=377117.58, hover_hightlight_polygon=None, bgcolor="#B3C0EF", textcolor="#000000")

    if "title" in param_dict:
        title = param_dict['title']
    else:
        title = default_param_dict['title']

    if "description" in param_dict:
        description = param_dict['description']
    else:
        description = default_param_dict['description']

    if "title_in_page" in param_dict:
        title_in_page = param_dict['title_in_page']
    else:
        title_in_page = default_param_dict['title_in_page']

    if "canvas_width" in param_dict:
        canvas_width = param_dict['canvas_width']
    else:
        canvas_width = default_param_dict['canvas_width']

    if "canvas_height" in param_dict:
        canvas_height = param_dict['canvas_height']
    else:
        canvas_height = default_param_dict['canvas_height']

    if "xmin" in param_dict:
        xmin = param_dict['xmin']
    else:
        xmin = default_param_dict['xmin']

    if "ymin" in param_dict:
        ymin = param_dict['ymin']
    else:
        ymin = default_param_dict['ymin']

    if "boxwidth" in param_dict:
        boxwidth = param_dict['boxwidth']
    else:
        boxwidth = default_param_dict['boxwidth']

    if "boxheight" in param_dict:
        boxheight = param_dict['boxheight']
    else:
        boxheight = default_param_dict['boxheight']

    if "hover_hightlight_polygon" in param_dict:
        hover_hightlight_polygon = param_dict['hover_hightlight_polygon']
    else:
        hover_hightlight_polygon = default_param_dict['hover_hightlight_polygon']

    if "bgcolor" in param_dict:
        bgcolor = param_dict['bgcolor']
    else:
        bgcolor = default_param_dict['bgcolor']

    if "textcolor" in param_dict:
        textcolor = param_dict['textcolor']
    else:
        textcolor = default_param_dict['textcolor']

    vmapperpath = os.path.dirname(__file__)
    templateLoader = jinja2.FileSystemLoader( searchpath=vmapperpath )
    # "./calc_frame/utils/exporter/vmapper"
    templateEnv = jinja2.Environment( loader=templateLoader )
    TEMPLATE_FILE = "svg_template_01.svg"
    template = templateEnv.get_template( TEMPLATE_FILE )
    templateVars = { "title" : title,
                     "description" : description,
                     "title_in_page" : title_in_page,
                     "canvas_width" : canvas_width,
                     "canvas_height": canvas_height,
                     "xmin": xmin,
                     "ymin": ymin,
                     "boxwidth" : boxwidth,
                     "boxheight" : boxheight,
                     "hover_hightlight_polygon" : hover_hightlight_polygon,
                     "bgcolor" : bgcolor,
                     "textcolor" : textcolor,
                     "drawing" : drawing,
                     "CDATA" : CDATA,
                     "textbox" : textbox,
                     "footer" : footer,
                    }
    outputText = template.render( templateVars )
    return outputText

def add_bike(path_id='', start_t='0s', dur='6s'):
    strings = '<circle cx="" cy="" r="20" fill="#FF0022">\n \
        <animateMotion begin="%s" dur="%s" repeatCount="1">\n \
           <mpath xlink:href="#%s"/>\n \
        </animateMotion>\n \
    </circle>'%(start_t, dur, path_id)
    return strings

def add_textbox():
    strings = ' <g>\n \
       <rect x="40" y="140" width="300" height="230" style="fill:rgb(200,200,200); fill-opacity:0.2" />\n \
       <text x="60" y="180" font-family="Calibri" font-size="20" fill="white" >Legend </text>\n \
       <circle cx="65" cy="205" r="10" fill="#FF0000" />\n \
       <text x="90" y="210" font-family="Calibri" font-size="14" fill="white" > chained subcluster </text>\n \
       <circle cx="65" cy="235" r="10" fill="#FFFF00" />\n \
       <text x="90" y="240" font-family="Calibri" font-size="14" fill="white" > solo subcluster </text>\n \
       <circle cx="65" cy="265" r="10" fill="#c8c8c8" />\n \
       <text x="90" y="270" font-family="Calibri" font-size="14" fill="white" > independent case </text>\n \
       <text x="60" y="330" font-family="Calibri" font-size="14" fill="white" > - location: Taiwan </text>\n  </g>'
    return strings
    #<text x="60" y="300" font-family="Calibri" font-size="14" fill="white" > - data period: 2009/7 - 2010/4</text>\n \

def add_footer():
    strings = ' <g>\n \
    <text x="1%" y="98%" font-family="Calibri" font-size="14" fill="white" >  Benny Chin - Lab. for Geospatial Computational Science, Dept. Geography, National Taiwan University </text> \n</g>'
    return strings

class simple_scene():
    def __init__(self, param_dict):
        self.params = param_dict
        self.layers = []
        self.style_items = []
        self.string_codes = []
        self.textboxs = ""

    def update_param(self, adict):
        self.params.update(adict)

    def add_Layer(self, layer, layername='layername', classkey='classkey', do=None, dsc=None, dsw=None):
        a = svg.initLayer(title=layername, key=classkey, dopacity=do, dscolor=dsc, dswidth=dsw)
        b = layer
        c = svg.closeLayer()
        self.layers.extend([a,b,c])

    def add_simpleLayer(self, layers):
        self.layers.extend(layers)

    def add_strcode(self, item):
        self.string_codes.append(item)

    def update_style(self, classkey="", featuretype="",hovercolor=None,hoveropacity=None,hoverstroke=None,hoverswidth=None):

        st = ['.%s :hover {\n'%(classkey)]
        if hovercolor is not None:
            st.append('fill: %s;\n'%(svg.colorstr(hovercolor)))
        if hoveropacity is not None:
            st.append('opacity: %s;\n'%(hoveropacity))
        if hoverstroke is not None:
            #print hoverstroke
            st.append('stroke:%s;\n'%(svg.colorstr(hoverstroke)))
        if hoverswidth is not None:
            st.append('stroke-width:%s;'%(hoverswidth))
        st.append('}\n')
        self.style_items.extend(st)

    def add_text(self, textstr):
        self.textboxs = textstr


    def render(self):
        strs = []
        for l in self.layers:
            slist = l.strarray()
            strs.extend(slist)
        strs.extend(self.string_codes)
        draws = " ".join(strs)

        style_items = " ".join(self.style_items)

        textboxs = self.textboxs

        footers = add_footer()

        return render_svg(param_dict=self.params, drawing=draws, CDATA=style_items, textbox=textboxs, footer=footers)
"""
def add_cluster_appear(xcor, ycor, t0, t1, t2):
    dur1 = str(t1-t0)+'s'
    dur1 = str(t2-t1)+'s'
    strings = '<circle cx="" cy="" r="20" fill="#FF0022">\n \
        <animateMotion begin="%s" dur="%s" repeatCount="1">\n \
           <mpath xlink:href="#%s"/>\n \
        </animateMotion>\n \
    </circle>'%(start_t, dur, path_id)
    return strings
"""
def grouping(val):
    if val >= 0.8:
        return 5
    elif val >= 0.6:
        return 4
    elif val >= 0.4:
        return 3
    elif val >= 0.2:
        return 2
    elif val >= 0:
        return 1
    else:
        return 0
