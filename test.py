
import pandas as pd
#import geopandas as gpd
#from shapely.geometry import Point

import TaPiTaS


def run_dist(gdf, s_radius, outpref):
    pts_setting = {'xcor':'xx', 'ycor':'yy', 'time':'time'}
    #s_radius = 2400
    T1 = 6
    T2 = 23
    PG_graph = TaPiTaS.Point_Diffusion(gdf, pts_setting = pts_setting, s_radius=s_radius, T1=T1, T2=T2, resample_time=99, confidence_level='0.80', critical_value=None)
    print("calculation done")
    #outf = 'run4/test1'
    #print(outpref)
    res = PG_graph.results
    print(res.get_summary_df())
    print(res.get_node_df())
    print(res.get_cluster_df())
    print(res.get_progress_df())
    print(res.get_final_slinks_df())
    print(res.get_final_nlinks_df())
    #PG_graph.to_csv(filename_prefix=outpref)
    #print("exported to:", outpref)

def main():
    #gdf = gpd.read_file()
    df = pd.read_csv('test_data/demo_0905.csv', index_col=0)
    outpref = 'temp/temp'
    print(df.head())
    run_dist(df, 5000, outpref)

if __name__ == '__main__':
    import sys
    print(sys.version)
    import time
    #print(time.time())
    #time_check_point = time.time()
    #time_now = time_check_point
    #time_check_point = time.time()
    #print(time_check_point - time_now)/float(60)
    #time_now = time_check_point
    main()
